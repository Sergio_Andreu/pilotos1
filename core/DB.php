<?php

declare(strict_types=1);

class DB
{
    private PDO $conexion;

    public function __construct()
    {
        $opciones = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        $string_conexion = 'mysql:host=' . DB_HOST . ';port=' . DB_PUERTO . ';dbname=' . DB_NOMBRE . ';charset=' . DB_CHARSET;

        try {
            $this->conexion = new PDO($string_conexion, DB_USUARIO, DB_PASSWORD, $opciones);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), $e->getCode());
        }
    }

    public function ejecutar(string $sql, ?array $parametros = null): PDOStatement
    {
        if (!$parametros) {
            return $this->conexion->query($sql);
        }
        $sentencia = $this->conexion->prepare($sql);
        $sentencia->execute($parametros);
        return $sentencia;
    }

    public function ultimoIdInsertado(): int
    {
        return (int) $this->conexion->lastInsertId();
    }

    public function comenzarTransacccion(): bool
    {
        return $this->conexion->beginTransaction();
    }

    public function commit(): bool
    {
        return $this->conexion->commit();
    }

    public function rollback(): bool
    {
        return $this->conexion->rollBack();
    }
}
