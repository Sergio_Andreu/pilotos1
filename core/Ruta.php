<?php

declare(strict_types=1);

class Ruta
{
    private string $regex_uri;
    private string $modelo;
    private string $vista;
    private ?string $controlador;
    private ?string $metodo;

    public function __construct(string $regex_uri, string $modelo, string $vista, ?string $controlador, ?string $metodo)
    {
        $this->regex_uri = $regex_uri;
        $this->modelo = $modelo;
        $this->vista = $vista;
        $this->controlador = $controlador;
        $this->metodo = $metodo;
    }

    public function uriEncaja(string $uri): bool
    {
        if (preg_match($this->regex_uri, $uri) === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getRegex(): string
    {
        return $this->regex_uri;
    }

    public function getModelo(): string
    {
        return $this->modelo;
    }

    public function getVista(): string
    {
        return $this->vista;
    }

    public function getControlador(): ?string
    {
        return $this->controlador;
    }

    public function getMetodo(): ?string
    {
        return $this->metodo;
    }
}
