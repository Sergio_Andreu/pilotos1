<?php

declare(strict_types=1);

/**
 * Clase que representará un stream de datos, envolviendo un stream de PHP tal y
 * como requerirán las clases Request y Response al implementar interfaces hijas
 * de MessageInterface.
 */
class Stream implements StreamInterface
{
}