<?php

declare(strict_types=1);

/**
 * Clase que representará una URI tal y como requerirá la clase Request al
 * implementar RequestInterface.
 */
class Uri implements UriInterface
{
}