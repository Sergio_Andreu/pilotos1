<?php

declare(strict_types=1);

class Despachador
{
    private array $rutas;

    public function __construct()
    {
        $this->rutas = [];
    }

    public function cargarRutas(array $definiciones_rutas): void
    {
        foreach ($definiciones_rutas as $def_ruta) {
            $ruta = new Ruta($def_ruta[0], $def_ruta[1], $def_ruta[2], $def_ruta[3], $def_ruta[4]);
            $this->rutas[] = $ruta;
        }
    }

    public function resolver(Request $request)
    {
        $request_uri = $request->getUri();

        if (!str_ends_with($request_uri, '/')) {
            $request_uri .= '/';
        }

        try {
            $hay_match = false;
            foreach ($this->rutas as $ruta) {
                if ($ruta->uriEncaja($request_uri)) {
                    $hay_match = true;
                    $ruta_coincidente = $ruta;
                    break;
                }
            }
            if ($hay_match) {
                $nombre_modelo = $ruta_coincidente->getModelo();
                $nombre_vista = $ruta_coincidente->getVista();
                $nombre_controlador = $ruta_coincidente->getControlador();
                $metodo = $ruta_coincidente->getMetodo();

                $db = new DB();
                $modelo = new $nombre_modelo($db);

                if (!is_null($nombre_controlador) && !is_null($metodo)) {
                    $controlador = new $nombre_controlador($modelo);
                    $controlador->$metodo($request);
                }
                $vista = new $nombre_vista($modelo);

                return $vista->render();
            } else {
                throw new Exception('Ruta no válida');
            }
        } catch (Exception $e) {
            if ($e->getMessage() === 'Ruta no válida') {
                http_response_code(404);
                include DIR_RAIZ . '/views/404.php';
                exit;
            } else {
                throw $e;
            }
        }
    }
}
