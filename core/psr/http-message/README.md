PSR Http Message
================

This repository holds all interfaces/classes/traits related to
[PSR-7](http://www.php-fig.org/psr/psr-7/).

Note that this is not a HTTP message implementation of its own. It is merely an
interface that describes a HTTP message. See the specification for more details.

Usage
-----

Before reading the usage guide we recommend reading the PSR-7 interfaces method list:

* [`PSR-7 Interfaces Method List`](docs/PSR7-Interfaces.md)
* [`PSR-7 Usage Guide`](docs/PSR7-Usage.md)

Atención
--------

Esta versión de las interfaces ha sido (o será) modificada para incluir tipado de
argumentos y returns de las funciones.

El PSR-7 se publicó pensado para PHP 5.3 y no ha sido actualizado a las nuevas
versiones del lenguaje debido a las preocupaciones de romper la retrocompatibilidad
con librerías que lo estuvieran implementando.

El Equipo de PHP-FIG es consciente de esto
(https://www.php-fig.org/blog/2019/10/upgrading-psr-interfaces/), pero aún no han
encontrado una solución adecuada para publicar una nueva versión oficial.