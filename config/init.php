<?php

declare(strict_types=1);
require 'config.php';
# Core
require DIR_RAIZ . '/core/DB.php';
require DIR_RAIZ . '/core/Despachador.php';
require DIR_RAIZ . '/core/Request.php';
require DIR_RAIZ . '/core/Ruta.php';
# Modelos
// require DIR_RAIZ . '/models/ParticipanteForm.php';
// require DIR_RAIZ . '/models/ParticipanteList.php';
// require DIR_RAIZ . '/models/Sorteo.php';
# Vistas
// require DIR_RAIZ . '/views/View.php';
// require DIR_RAIZ . '/views/ParticipanteForm.php';
// require DIR_RAIZ . '/views/ParticipanteList.php';
// require DIR_RAIZ . '/views/Sorteo.php';
# Controladores
// require DIR_RAIZ . '/controllers/ParticipanteForm.php';
// require DIR_RAIZ . '/controllers/ParticipanteList.php';
// require DIR_RAIZ . '/controllers/Sorteo.php';
