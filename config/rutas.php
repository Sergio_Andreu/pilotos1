<?php

$rutas = [
    # Expresión Regular URI                Modelo                      Vista                      Controlador                      Método Cont.
    ['/^\/$/',                             'SorteoModel',              'IndexView',               null,                            null],
    ['/^\/participante\/$/',               'ParticipanteFormModel',    'ParticipanteFormView',    null,                            null],
    ['/^\/participante\/inscribir\/$/',    'ParticipanteFormModel',    'ParticipanteFormView',    'ParticipanteFormController',    'inscribir'],
    ['/^\/lista\/$/',                      'ParticipanteListModel',    'ParticipanteListView',    'ParticipanteListController',    'lista'],
    ['/^\/sortear\/$/',                    'SorteoModel',              'SorteoView',              'SorteoController',              'sortear'],
    ['/^\/resetear\/$/',                   'SorteoModel',              'SorteoView',              'SorteoController',              'resetear']
];

return $rutas;
